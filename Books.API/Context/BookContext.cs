﻿using Books.API.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Books.API.Context
{
    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        public BookContext(DbContextOptions<BookContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>().HasData(

                new Author()
                {

                    Id = Guid.Parse("e3f7e953-b0ae-4e14-8b01-eaf4a177fd04"),
                    FirstName = "John",
                    LastName = "Mclane"

                },
                 new Author()
                 {

                     Id = Guid.Parse("c889a9e6-6337-4f34-b0c9-fb322bee5378"),
                     FirstName = "John",
                     LastName = "Stuart"

                 },
                  new Author()
                  {

                      Id = Guid.Parse("4e599668-3757-40a5-a864-da8f4f6b9a08"),
                      FirstName = "Sue",
                      LastName = "Mclane"

                  });

            modelBuilder.Entity<Book>().HasData(

                new Book()
                {

                    Id = Guid.Parse("c2ba4426-7a5c-4b59-9de6-f4bbf2fb9807"),
                    AuthorId = Guid.Parse("c889a9e6-6337-4f34-b0c9-fb322bee5378"),
                    Title = "A game of Thrones",
                    Description = "Just like the TV Show Game of Thrones"

                },
                new Book()
                {

                    Id = Guid.Parse("ea4a57af-e08e-4d45-90e3-38d5a0e17572"),
                    AuthorId = Guid.Parse("c889a9e6-6337-4f34-b0c9-fb322bee5378"),
                    Title = "Lord of Rings",
                    Description = "Movie based lord of the rings"

                },
                new Book()
                {

                    Id = Guid.Parse("50b77044-3230-4ad8-bf7a-1ac03b2067e5"),
                    AuthorId = Guid.Parse("e3f7e953-b0ae-4e14-8b01-eaf4a177fd04"),
                    Title = "A game of Thrones",
                    Description = "Just like the TV Show Game of Thrones"

                },
                new Book()
                {

                    Id = Guid.Parse("825be6fb-49a4-4ff8-a331-ebe0c401a620"),
                    AuthorId = Guid.Parse("e3f7e953-b0ae-4e14-8b01-eaf4a177fd04"),
                    Title = "Captain Planet",
                    Description = "Just like the TV Show Game of Thrones"

                },
                new Book()
                {

                    Id = Guid.Parse("898328b8-4208-4ce6-903b-cdb9f27e5297"),
                    AuthorId = Guid.Parse("4e599668-3757-40a5-a864-da8f4f6b9a08"),
                    Title = "Rambo",
                    Description = "John Rambo again"

                },
                new Book()
                {

                    Id = Guid.Parse("e23574d0-4153-492e-85c7-0670a62b958b"),
                    AuthorId = Guid.Parse("4e599668-3757-40a5-a864-da8f4f6b9a08"),
                    Title = "Little Poney",
                    Description = "All about young poney"

                });

            base.OnModelCreating(modelBuilder);
        }
    }
}
