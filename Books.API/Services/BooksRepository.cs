﻿using Books.API.Context;
using Books.API.Entities;
using Books.API.ResourceParameters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Books.API.Services
{
    public class BooksRepository : IBookRepository, IDisposable
    {

        BookContext _context;

        public BooksRepository(BookContext context)
        {
            _context = context ?? throw new ArgumentException(nameof(context));
        }



        public async Task<Book> GetBookAsync(Guid id)
        {
            return await _context.Books.Include(b => b.Author).FirstOrDefaultAsync(b => b.Id == id);
        }

        //[Obsolete]
        public async Task<IEnumerable<Book>> GetBooksAsync()
        {

            return await _context.Books.Include(b => b.Author).ToListAsync();
        }

        public async Task<IEnumerable<Book>> GetBooksAsync(BookResourceParameters search)
        {

            if (search == null)
            {
                throw new ArgumentNullException(nameof(search));
                
            }

            if (string.IsNullOrWhiteSpace(search.Title) && string.IsNullOrWhiteSpace(search.Description) && string.IsNullOrEmpty(search.Author))
            {
                return await _context.Books.Include(b => b.Author).ToListAsync();
            }

            return _context.Books.Include(b => b.Author).Where(x => x.Description.Contains(search.Description)
             || x.Author.FirstName.Contains(search.Author) 
             || x.Author.LastName.Contains(search.Author) 
             || x.Title.Contains(search.Title)).ToList();

            //return await _context.Books.Include(b => b.Author).ToListAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {


            if (disposing)
            {

                if (_context != null)
                {

                    _context.Dispose();
                    _context = null;

                }

            }

        }
    }
}
