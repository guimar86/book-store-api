﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Books.API.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 150, nullable: false),
                    LastName = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 150, nullable: false),
                    Description = table.Column<string>(maxLength: 150, nullable: true),
                    AuthorId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Books_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { new Guid("e3f7e953-b0ae-4e14-8b01-eaf4a177fd04"), "John", "Mclane" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { new Guid("c889a9e6-6337-4f34-b0c9-fb322bee5378"), "John", "Stuart" });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { new Guid("4e599668-3757-40a5-a864-da8f4f6b9a08"), "Sue", "Mclane" });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "AuthorId", "Description", "Title" },
                values: new object[,]
                {
                    { new Guid("50b77044-3230-4ad8-bf7a-1ac03b2067e5"), new Guid("e3f7e953-b0ae-4e14-8b01-eaf4a177fd04"), "Just like the TV Show Game of Thrones", "A game of Thrones" },
                    { new Guid("825be6fb-49a4-4ff8-a331-ebe0c401a620"), new Guid("e3f7e953-b0ae-4e14-8b01-eaf4a177fd04"), "Just like the TV Show Game of Thrones", "Captain Planet" },
                    { new Guid("c2ba4426-7a5c-4b59-9de6-f4bbf2fb9807"), new Guid("c889a9e6-6337-4f34-b0c9-fb322bee5378"), "Just like the TV Show Game of Thrones", "A game of Thrones" },
                    { new Guid("ea4a57af-e08e-4d45-90e3-38d5a0e17572"), new Guid("c889a9e6-6337-4f34-b0c9-fb322bee5378"), "Movie based lord of the rings", "Lord of Rings" },
                    { new Guid("898328b8-4208-4ce6-903b-cdb9f27e5297"), new Guid("4e599668-3757-40a5-a864-da8f4f6b9a08"), "John Rambo again", "Rambo" },
                    { new Guid("e23574d0-4153-492e-85c7-0670a62b958b"), new Guid("4e599668-3757-40a5-a864-da8f4f6b9a08"), "All about young poney", "Little Poney" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Books_AuthorId",
                table: "Books",
                column: "AuthorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Authors");
        }
    }
}
