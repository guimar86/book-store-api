﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Books.API.Filter;
using Books.API.ResourceParameters;
using Books.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Books.API.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class BooksController : ControllerBase
    {
        //new changes
        private readonly IBookRepository _repository;
        public BooksController(IBookRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        [HttpGet]
        [BooksResultFilter()]
        public async Task<IActionResult> GetBooks([FromQuery] BookResourceParameters search)
        {
            try
            {

                var bookEntities = await _repository.GetBooksAsync(search);
                return Ok(bookEntities);
            }
            catch (Exception ex)
            {
                var problemDetails = new ProblemDetails
                {
                    Status=StatusCodes.Status403Forbidden,
                    Type=ex.GetType().ToString(),
                    Title = ex.Message,
                    Instance = HttpContext.Request.Path,
                    Detail =ex.StackTrace
                   
                };

                return BadRequest(problemDetails);
                
            }

            

        }

        [HttpGet]
        [Route("{id}")]
        [BookResultFilter()]
        public async Task<IActionResult> GetBook(Guid id)
        {

            var book = await _repository.GetBookAsync(id);

            if (book==null)
            {
                return NotFound();
            }

            return Ok(book);

        }
    }
}